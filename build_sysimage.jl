using PackageCompiler
using GLMakie
using AbstractPlotting
using MeshIO
using FileIO
using Revise
using ZMQ

create_sysimage(
    [:GLMakie, :AbstractPlotting, :MeshIO, :FileIO, :Revise, :ZMQ]; 
    # precompile_statements_file="custom_sysimage.jl", 
    sysimage_path="sysimage.so")
