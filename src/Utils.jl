module Utils
using AbstractPlotting
using MakieUtils
using GLMakie
using GLFW
using JSON3

using SpacecraftKinematics.Rotations

q321makie(a3, a2, a1) = AbstractPlotting.Quaternion(q321dxyzw(a3, a2, a1)...)
q123makie(a3, a2, a1) = AbstractPlotting.Quaternion(q321dxyzw(a1, a2, a3)...)

export q321makie, q123makie, setup_scene, ASSETS_FOLDER, SceneInfo, show_scene, @async_showerr

ASSETS_FOLDER = abspath(@__DIR__() * "/../assets/")
DEFAULT_RESOLUTION = (1500, 1500)

mutable struct SceneInfo
  # parentscene::Scene
  scene
  layout::GridLayout
  lscene3d::LScene
  angvel_vector::AbstractPlotting.LineSegments
  infotext
  # hbox::Scene
  # text::Scene
end

function setup_scene()
  # Initial Layout
  outer_padding = 20
  # set_theme!(backgroundcolor = :purple)
  set_theme!(backgroundcolor=RGBAf0(0.9, 0.9, 0.9, 1))
  fig = Figure(resolution = (1800, 1800)) #, backgroundcolor=:black)
  # layout[1,1] = GridLayout()
  # layout[1,2] = GridLayout()
  
  # aspect=AxisAspect(1.0), 
  textaxis = fig[1, 2] = Axis(fig; aspect=DataAspect(), xpanlock=true, ypanlock=true, xzoomlock=true, yzoomlock=true, xrectzoom=false, yrectzoom=false, backgroundcolor=RGBAf0(0,0,0,0))
  # 3D scene
  lscene3d = fig[1, 2] = LScene(fig, scenekw = (camera = cam3d!, raw = false, backgroundcolor=:black), tellheight=true)
  lscene3d.scene.clear = true

  
  origin_triad = triad!(lscene3d, 1; linewidth=2)
  # another_triad = triad!(lscene3d, 500; translation=Vec(100,100,0), show_axis=true)
  angvel_vector = vector!(lscene3d, 5; linewidth=4)
  
  hidedecorations!(textaxis)
  # label = fig[1,2, TopLeft()] = Label(fig, "Foo", color=:blue)
  infonode = Node("Hello")
  infotext = text!(textaxis, infonode; color=:green, textsize=1.0)
  
  textaxis.attributes[:targetlimits] = Rect2D(0, 0, 50.0f0, 50.0f0)
  
  # supertitle = fig[0, :] = Label(scene, "Spacecraft Attitude",
  #   textsize = 30, font = "Noto Sans Bold", color = (:black, 0.25))


  colsize!(fig.layout, 1, Relative(1/4))
  colsize!(fig.layout, 2, Relative(3/4))
  rowsize!(fig.layout, 1, Relative(1))
  SceneInfo(fig, fig.layout, lscene3d, angvel_vector, infotext)
end


function show_scene(scene; window_title="Spacecraft Visualization")
  display(scene)
  GLFW.SetWindowTitle(scene.scene.current_screens[1].glscreen, window_title)
end

function setwindowtitle(fig::AbstractPlotting.Figure, title)
  glscreen = fig.scene.current_screens[1].glscreen
  GLFW.SetWindowTitle(glscreen, title)
end
export setwindowtitle

# https://discourse.julialang.org/t/how-to-get-backtrace-of-async-task/18170/3?u=airpmb
macro async_showerr(ex)
  quote
      t = @async try
          eval($(esc(ex)))
      catch err
          bt = catch_backtrace()
          println()
          showerror(stderr, err, bt)
      end
  end
end


function loaddata(filename)
  json = JSON3.read(filename)
  N = length(json.tvec)
  states = reshape(Array{Float64}(json.data), (N,:))
  (;tvec=Float64.(json.tvec), σ=states[:,4:6], ω)
end
export loaddata

function mrp_shadow(σ)
  σ² = normsq(σ)
  if σ² > 1.0
    # @infiltrate
    return -σ/σ²
  else
    return σ
  end
end
export mrp_shadow

end