module AttitudeVisualizationZmqApp

using GLMakie
using MakieUtils
using ZMQ
using Rotations
using Rotations: params
using Parameters
using Printf
using DataStructures
using Statistics
using JSON3
using StaticArrays

using SpacecraftVisualization
using SpacecraftVisualization.Utils
using SpacecraftVisualization.AttitudeVisualization

spacecraft_info = Dict(
  "Voyager" => (mesh="17244_Voyager_2_v1_NEW.obj", translation=0.0254f0*Vec3f0(0, -32.5, -60), scale=0.0254),
  "New Horizons" => (mesh="new_horizons2.stl", translation=(0, 0, 0), scale=1.0),
)
DEFAULT_SPACECRAFT = "Voyager"

@with_kw mutable struct UIModel
  mrp = Node(zero(MRP))
  runloop = false
  # mrp_history::typeof(Node(CircularBuffer{MRP}(zeros(MRP, 100))))
  # UIModel() = new(Vec3f0(0), Node(Vec3f0(0)), Node(MRP(0.0,0.0,0.0)), Vec3f0(0), false)
  # UIModel(xyz_inch, ypr_deg, mrp, omega_dps) = new(xyz_inch, ypr_deg, mrp, omega_dps, false)
end

@with_kw mutable struct UIView
  ui_locked = false
  sceneinfo = nothing
  spacecraft = nothing
end

function set_ui_lock!(view::UIView, lockstate::Bool)
  view.ui_locked = lockstate
end

function lock_ui!(v::UIView)
  @debug "Locking UI"
  set_ui_lock!(v, true)
end

function unlock_ui!(v::UIView)
  @debug "Unlocking UI"
  set_ui_lock!(v, false)
end

function setup_scene()
  # Initial Layout
  outer_padding = 0
  # set_theme!(backgroundcolor = :purple)
  # set_theme!(backgroundcolor=RGBAf0(0.9, 0.9, 0.9, 1))
  fig = Figure(resolution = (1800, 1800)) #, backgroundcolor=:black)
  # layout[1,1] = GridLayout()
  # layout[1,2] = GridLayout()
  
  # aspect=AxisAspect(1.0), 
  textaxis = fig[1, 1] = Axis(fig; aspect=DataAspect(), xpanlock=true, ypanlock=true, xzoomlock=true, yzoomlock=true, xrectzoom=false, yrectzoom=false, backgroundcolor=RGBAf0(0,0,0,0))
  # 3D scene
  lscene3d = fig[1, 1] = LScene(fig, scenekw = (camera = cam3d!, raw = false, backgroundcolor=:black), tellheight=true)
  lscene3d.scene.clear = true

  
  origin_triad = triad!(lscene3d, 1; linewidth=2)
  # another_triad = triad!(lscene3d, 500; translation=Vec(100,100,0), show_axis=true)
  # angvel_vector = vector!(lscene3d, 5; linewidth=4)
  
  hidedecorations!(textaxis)
  # label = fig[1,2, TopLeft()] = Label(fig, "Foo", color=:blue)
  infonode = Node("Hello")
  infotext = text!(textaxis, infonode; color=:green, textsize=1.0)
  
  textaxis.attributes[:targetlimits] = Rect2D(0, 0, 50.0f0, 50.0f0)
  
  # supertitle = fig[0, :] = Label(scene, "Spacecraft Attitude",
  #   textsize = 30, font = "Noto Sans Bold", color = (:black, 0.25))


  # colsize!(fig.layout, 1, Relative(1/4))
  # colsize!(fig.layout, 2, Relative(3/4))
  # rowsize!(fig.layout, 1, Relative(1))
  # SceneInfo(fig, fig.layout, lscene3d, angvel_vector, infotext)
  display(fig)
  setwindowtitle(fig, "Spacecraft Attitude")
  (;fig, lscene3d, infotext)
end

function update_view!(uiview::UIView, uimodel::UIModel)
  # @show uimodel.mrp
  q = params(UnitQuaternion(uimodel.mrp[]))
  q2 = [q[2], q[3], q[4], q[1]]
  rotate!(uiview.spacecraft.triad, q2...)
end

function interaction_loop(uiview::UIView, uimodel::UIModel)
  uimodel.runloop && error("Loop already running")
  uimodel.runloop = true
  t = 0.0
  t_prev = 0.0
  dt = 0.1
  # ##@debug isopen(uiview.sceneinfo.scene)
  iloop = "interaction_loop"
  waiting = "$(iloop): Waiting for scene.."
  @info waiting
  while !isopen(uiview.sceneinfo.fig.scene) # wait for screen to be open
    @info waiting
    sleep(1)
  end
  @info "$(iloop): Scene ready"
  t = time()
  t0 = t
  loopcount = 0
  dt_buf = CircularBuffer{Float64}(100)
  while uimodel.runloop && isopen(uiview.sceneinfo.fig.scene)
    # @debug "top of run loop"
    uiview.sceneinfo.infotext.input_args[1][] = "t = $(@sprintf("%.1f", t - t0)) s; FPS = $(@sprintf("%.1f", 1/max(0.01, mean(dt_buf))))"
    # @debug "xxx"
    # loopcount > 100 && error("test error")
    loopcount += 1
    if loopcount % 50 == 0
      @info "Loop count $loopcount"
      @info uimodel.mrp
    end
    # @debug "xxx2"
    # @debug "Top of interaction loop t = $(t)"
    # @debug "uiview.ui_locked = $(uiview.ui_locked); $(ui_locked(uiview))"
    t_prev = t
    t = time()
    dt_meas = t - t_prev
    # @debug "xxx2"
    push!(dt_buf, dt_meas)
    # @debug "yyy"
    # @debug "Locking UI"
    lock_ui!(uiview)
    
    update_view!(uiview, uimodel)
    # @debug "After update_view!"
    unlock_ui!(uiview)
    # @debug "After unlock_ui! dt_meas=$dt_meas"
    # @debug "zzz"
    sleep(0.8*dt)
    # @debug "Bottom of run loop"
  end
  @warn "$iloop: exiting"
end

@with_kw mutable struct ZMQState
  run = true
end

ZMQPORT = 50042
ZMQSUBFILTERSTRING = "ATTITUDE"

RotMatrix2(c::AbstractArray) = Rotations.RotMatrix(SMatrix{3,3}(reshape(c, (3,3))))
# RotMatrix(c::Vector) = RotMatrix(reshape(c, (3,3)))

function zmqloop(state::ZMQState, uimodel::UIModel)
  @info "zmqloop foo"
  context = Context()
  socket = Socket(context, SUB)
  @show socket
  addr ="tcp://127.0.0.1:$ZMQPORT"
  @info "Connecting to $addr..."
  ZMQ.subscribe(socket, ZMQSUBFILTERSTRING)
  # ZMQ.set_rcvhwm(socket, 10)
  # ZMQ.set_rcvbuf(socket, 1024)
  ZMQ.connect(socket, addr)
  count = 0
  drainqueue = true
  lastmsgcount = 0
  msgcount = 0
  try
    while state.run
      @info "recv..."
      tbefore = time()
      data = String(ZMQ.recv(socket))
      count % 100 == 0 && (@info "Received $count messages")
      parts = split(data, "#")
      # @show parts[1], parts[2]
      @assert parts[1] == ZMQSUBFILTERSTRING
      msgcount = parse(Int, parts[2])
      payload = parts[3]
      # @show payload
      jdata = JSON3.read(payload)
      # @show jdata
      # @show jdata.mrp, typeof(jdata.mrp)
      rot = RotMatrix2(convert(AbstractVector{Float64}, jdata.mrp))
      # @show rot
      mrp = MRP(rot)
      # mrp = MRP(mrp_shadow(mrp)...)
      # @show mrp
      # @show params(mrp)
      # uimodel.mrp[] = mrp
      uimodel.mrp[] = MRP((count % 100)/100.0, 0, 0)
      @show AngleAxis(uimodel.mrp[]).theta
      @show uimodel.mrp[]
      # msgcount = jdata["count"]
      tafter = time()
      # @info count, String(data)
      msgcountdiff = msgcount - lastmsgcount
      lastmsgcount = msgcount
      if msgcountdiff > 100
        @warn "$msgcountdiff messages behind, catching up.."
        continue
      else
        # @info "$msgcountdiff"
      end
      if count % 1000 == 0
        @info "count=$count; msgcount=$msgcount; δt=$(tafter-tbefore); $jdata"
        # return payload
        # @info data
      end
      # drainqueue = drainqueue || (tafter - tbefore) < 0.000001
      # if drainqueue
      #   # wait until at least 1 msec wait for message
      #   @info "draining queue... δt=$(tafter-tbefore)"
      #   (tafter - tbefore) > 0.001 && (drainqueue = false)
      # else
      # if count % 10 == 0
      # end
      # process(scene, payload)
      yield()
      sleep(0.01)
      # end
      # end
      count += 1
    end
  catch e
    @error e
    if typeof(e) == StateError
      @warn "Got StateError"
    else
      rethrow(e)
    end
  finally
    @info "Shutting down ZMQ..."
    ZMQ.close(socket)
    ZMQ.close(context)
    context = nothing
  end
end
export zmqloop

function main()
  sceneinfo = setup_scene()
  spacecraft = addspacecraft!(sceneinfo.lscene3d.scene, Node(spacecraft_info[DEFAULT_SPACECRAFT]))
  uiview = UIView(; sceneinfo, spacecraft)
  uimodel = UIModel(; )
  (;uiview, uimodel)
end
export main

end

ret = AttitudeVisualizationZmqApp.main()