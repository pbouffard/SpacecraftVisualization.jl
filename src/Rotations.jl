module SpacecraftVisualizationRotations
using Rotations
import Rotations: params
using LinearAlgebra

export mrp_shadow

"""
Return shadow MRP set if norm is close to 1
"""
function mrp_shadow(σ::MRP)
  norm_σ = norm(σ)
  norm_σ > 0.99 ? MRP(params(σ)/norm_σ) : σ
end
  
end