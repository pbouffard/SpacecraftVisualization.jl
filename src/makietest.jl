set_theme!(backgroundcolor=RGBAf0(0.9, 0.9, 0.9, 1))

F = let
  fig = Figure(resolution = (1800, 1800))
  scenekw = (camera = cam3d!, raw = false, backgroundcolor=:black) # this backgroundcolor not used?
  lscene3d = fig[1, 2] = LScene(fig, scenekw=scenekw)#, tellheight=true)
  # colsize!(fig.layout, 1, Relative(1/4))
  # colsize!(fig.layout, 2, Relative(3/4))
  gl = fig.layout[1,1] = GridLayout()
  bu = gl[1,1] = Button(fig; label="Foo")
  kwargs = (;)
  # kwargs = (;backgroundcolor=:purple) # supersedes backgroundcolor given in scenekw above
  m = mesh!(lscene3d, load("assets/new_horizons2.stl"); alpha=0.5, color=(:grey, 0.5), show_axis=false, kwargs...)
  lscene3d.scene.clear = true
  lscene3d2 = fig[2, 2] = LScene(fig, scenekw=(camera = cam3d!, raw = false, backgroundcolor=:blue))#, tellheight=true)
  lscene3d2.scene.clear = true
  m2 = mesh!(lscene3d2, load("assets/new_horizons2.stl"); alpha=0.5, color=(:blue, 0.5), show_axis=false, kwargs...)
  rowsize!(fig.layout, 1, Relative(1/2))
  rowsize!(fig.layout, 2, Relative(1/2))
  fig
end

F