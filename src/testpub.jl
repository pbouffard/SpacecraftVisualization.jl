module TestPub

using ZMQ
using JSON3
# using Random
# using StaticArrays
# using Colors
using Rotations
using SpacecraftVisualization.Utils


ZMQPORT = 50042
ZMQSUBFILTERSTRING = "ATTITUDE"


function main(;dt=0.01, dtsim=dt)
  SOCKET_ADDR="tcp://127.0.0.1:$ZMQPORT"
  
  context = ZMQ.Context()
  socket = ZMQ.Socket(context, PUB)
  # @show fieldnames(socket)
  socket.sndhwm = 1
  socket.linger = 0
  # socket.sndbuf = 1024
  @show socket
  @info "Binding to $SOCKET_ADDR"
  rc = ZMQ.bind(socket, SOCKET_ADDR)
  @show rc
  # rc = ZMQ.bind(socket, "ipc://test.ipc")
  @show rc, socket
  
  N = 100
  
  ϕ = 0.0
  dϕdt = deg2rad(10)
  mrp = zero(MRP)
  try
    count = 0
    t0 = time()
    tsim = 0.0
    t = 0.0
    twall = t0+0.001
    while true #count < N*1000
      sleep(dt)
      tsim += dtsim
      ϕ = tsim * dϕdt
      @show tsim, ϕ
      t += dt
      count += 1
      # count % N == 0 && sleep(0.01)
      # data = JSON3.write((; name="foo", position=zeros(3)))
      i = rand(1:N)
      mrp *= RotX(ϕ)
      mrp = mrp_shadow(mrp)
      data = JSON3.write((; count, mrp))
      # @show datas
      # for data in datas
      msgstr = "$ZMQSUBFILTERSTRING#$count#" * data
      @show msgstr
      msg = ZMQ.Message(msgstr)
      ZMQ.send(socket, msg)
      # end
      twall = time()
      yield()
      if count % N == 0
        @info "Count = $count; msg/sec = $(count/(twall-t0)) Sending: $(msgstr)"
        # sleep(0.001)
      end
    end
  catch e
    @warn "Caught error.. $e"
    if typeof(e) == StateError
      @warn "Got StateError"
    else
      rethrow(e)
    end
  finally
    @info "Shutting down ZMQ..."
    ZMQ.close(socket)
    ZMQ.term(context)
    ZMQ.close(context)
    context = nothing
  end
end
export main

end

TestPub.main(; dt=0.1)

