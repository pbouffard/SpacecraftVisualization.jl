module AttitudeVisualization
using GLMakie
using ColorSchemes
using AbstractPlotting
using AbstractPlotting: rotate!
using LinearAlgebra: normalize, norm
using Printf
using Rotations
using Parameters
using DataStructures
using Statistics
# import Rotations: params
# using ..SpacecraftVisualizationRotations: mrp_shadow
# using ..Utils: show_scene, @async_showerr
# using InteractiveUtils

using MakieUtils
using FileIO
using Observables: on

using SpacecraftVisualization

using ..Utils
using SpacecraftKinematics.Rotations: DCM2MRP, C321d, quat2DCM, C321⁻¹, DCM2quat, DCM2PRV, pointingDCM, quat_wxyz2xyzw, PRV2DCM, MRP2DCM, dσdt_mrp

export animatefromdata!

export interact


spacecraft_info = Dict(
  "Voyager" => (mesh="17244_Voyager_2_v1_NEW.obj", translation=0.0254f0*Vec3f0(0, -32.5, -60), scale=0.0254),
  "New Horizons" => (mesh="new_horizons2.stl", translation=(0, 0, 0), scale=1.0),
)
DEFAULT_SPACECRAFT = "Voyager"

format3decimal(x, unit="") = "$(@sprintf("%0.3f", x))$(unit)"

mutable struct UIModel
  xyz_inch::typeof(Vec3(1.0))
  ypr_deg::typeof(Node(Vec3(1.0)))
  mrp::typeof(Node(MRP(0.0,0.0,0.0)))
  omega_dps::typeof(Vec3(1.0))
  runloop::Bool
  # mrp_history::typeof(Node(CircularBuffer{MRP}(zeros(MRP, 100))))
  UIModel() = new(Vec3f0(0), Node(Vec3f0(0)), Node(MRP(0.0,0.0,0.0)), Vec3f0(0), false)
  UIModel(xyz_inch, ypr_deg, mrp, omega_dps) = new(xyz_inch, ypr_deg, mrp, omega_dps, false)
end

mutable struct UIView
  ui_locked::Bool
  sceneinfo::SceneInfo
  # TODO: can the following be given concrete types?
  vehiclemenu
  yprsliders
  quatsliders
  prvsliders
  mrpsliders
  angvelsliders
  dcmaxis
  mrpaxis
  zerobutton
  mesh
  UIView(sceneinfo, vehiclemenu, yprsliders, quatsliders, prvsliders, mrpsliders, angvelsliders, dcmaxis, mrpaxis, zerobutton, mesh) = new(false, sceneinfo, vehiclemenu, yprsliders, quatsliders, prvsliders, mrpsliders, angvelsliders, dcmaxis, mrpaxis, zerobutton, mesh)
end

function set_ui_lock!(view::UIView, lockstate::Bool)
  view.ui_locked = lockstate
end

function lock_ui!(v::UIView)
  @debug "Locking UI"
  set_ui_lock!(v, true)
end
function unlock_ui!(v::UIView)
  @debug "Unlocking UI"
  set_ui_lock!(v, false)
end

ui_locked(v::UIView) = v.ui_locked

export addspacecraft!
function addspacecraft!(scene, spacecraft_node; loc=(0,0,0), rot::Observable{Quaternionf0}=Node(Quaternionf0(0,0,0,1))) #q321makie(0,0,0))
  # spacecraft_node = Node(spacecraft)
  meshdata = lift(s -> load(ASSETS_FOLDER * s.mesh), spacecraft_node)
  t = Scene(scene)
  vvframe = triad!(t, 2; linewidth=4)
  AbstractPlotting.rotate!(vvframe, q321makie(0,0,0))
  # TODO: scale to meters
  m = mesh!(t, meshdata; alpha=0.5, color=(:grey, 0.5), show_axis=false) #, backgroundcolor=:purple) #[end]
  # ##@debug typeof(m)
  # scale = lift(s -> (s.scale, s.scale, s.scale), spacecraft_node)
  on(spacecraft_node) do s
    @info "on(spacecraft_node)"
    @show s
    scale!(m, (s.scale, s.scale, s.scale))
    translate!(m, s.translation)
  end
  # spacecraft_node[] = spacecraft_node[]
  (;mesh=spacecraft_node, triad=t)
end

function readouttexts()
  txts = ["β₀", "β₁", "β₂", "β₃"]
  [text(txt * ":"; show_axis=false) for txt in txts]
end

function update_ypr!(uimodel::UIModel, uiview::UIView)
  ypr = [s.value[] for s in uiview.yprsliders.sliders]
  uimodel.ypr_deg[] = ypr
end

function update_quat!(uimodel::UIModel, uiview::UIView)
  quat = [s.value[] for s in uiview.quatsliders.sliders]
  quat ≈ zeros(4) && (quat = [1 0 0 0])
  quat = normalize(quat)
  C = quat2DCM(quat)
  ypr_deg = DCM2ypr_deg(C)
  uimodel.ypr_deg[] = ypr_deg
end

function update_prv!(uimodel::UIModel, uiview::UIView)
  prv_params = [s.value[] for s in uiview.prvsliders.sliders]
  Φ = deg2rad(first(prv_params))
  v = prv_params[2:4]
  v ≈ zeros(3) && (v = [1 0 0])
  ê = normalize(v)
  C = PRV2DCM(Φ, ê)
  uimodel.ypr_deg[] = DCM2ypr_deg(C)
end

function update_mrp!(uimodel::UIModel, uiview::UIView)
  σ = MRP([s.value[] for s in uiview.mrpsliders.sliders]...)
  # σ = mrp_shadow(σ)
  C = RotMatrix(σ)
  ypr = RotXYZ(σ)
  # ##@debug [ypr.theta3, ypr.theta2, ypr.theta1]
  uimodel.ypr_deg[] = rad2deg.([ypr.theta3, ypr.theta2, ypr.theta1])
  uimodel.mrp[] = σ
  # ##@debug uimodel.mrp
end

function update_angvel!(uimodel::UIModel, uiview::UIView)
  omega_dps = [s.value[] for s in uiview.angvelsliders.sliders]
  # @debug omega_dps
  # @debug uimodel.omega_dps
  uimodel.omega_dps = Vec3(omega_dps)
  @debug omega_dps, uimodel.omega_dps
end

function DCM2ypr_deg(C)
  rad2deg.(collect(C321⁻¹(C)))
end

function update_view!(uiview::UIView, uimodel::UIModel)
  #@debug "In update_view!"
  # C_BN = C321d(uimodel.ypr_deg[]...)
  C_BN = Matrix(uimodel.mrp[])
  quat = DCM2quat(C_BN)
  #@debug "quats"
  for i=1:4
    set_close_to!(uiview.quatsliders.sliders[i], quat[i])
  end
  
  ##@debug "yprs"
  for i=1:3
    set_close_to!(uiview.yprsliders.sliders[i], uimodel.ypr_deg[][i])
  end
  
  ##@debug "prv"
  prv = DCM2PRV(C_BN)
  set_close_to!(uiview.prvsliders.sliders[1], rad2deg(prv.Φ))
  for i=2:4
    set_close_to!(uiview.prvsliders.sliders[i], prv.ê[i-1])
  end
  
  #@debug "mrp"
  σ = [uimodel.mrp[].x, uimodel.mrp[].y, uimodel.mrp[].z] # TODO: params(::MRP) missing?
  # @show σ
  for i=1:3
    set_close_to!(uiview.mrpsliders.sliders[i], σ[i])
  end
  
  #@debug "angvel"
  for i=1:3
    set_close_to!(uiview.angvelsliders.sliders[i], uimodel.omega_dps[i])
  end
  
  # ##@debug supertypes(typeof(uiview.mesh.triad))
  rotate!(uiview.mesh.triad, q321makie(uimodel.ypr_deg[]...))
  ω_B = deg2rad.(uimodel.omega_dps)
  ω_N = C_BN' * ω_B
  # ##@debug norm(ω)
  norm(ω_N) ≈ 0 && (ω_N = [1; 0; 0])
  Cω_N = pointingDCM(ω_N)' # DCM from inertial to ω in N frame
  # ##@debug Cω_N
  rotate!(uiview.sceneinfo.angvel_vector, Quaternion(quat_wxyz2xyzw(DCM2quat(Cω_N))...))
  #@debug "Leaving update_view!"
end

function reset_rotation!(uimodel::UIModel)
  uimodel.ypr_deg[] = Vec3(0)
  uimodel.mrp[] = one(MRP)
  uimodel.omega_dps = Vec3(0)
end

export attitude_viz_scene
function attitude_viz_scene()
  sceneinfo = setup_scene()
  
  spacecraft = addspacecraft!(sceneinfo.lscene3d.scene, Node(spacecraft_info[DEFAULT_SPACECRAFT]))
  # ##@debug vger

  vehiclemenu = Menu(sceneinfo.scene, options=[(k, v) for (k, v) in spacecraft_info]; prompt="Select Vehicle...")

  yprrange = Ref(LinRange(-180:0.01:180))
  yprsliders = labelslidergrid!(sceneinfo.scene, ["ψ", "θ", "ϕ"], yprrange; formats = x -> format3decimal(x, "°")) #, tellheight=false)
  
  quatrange = Ref(LinRange(-1.0:0.01:1.0))
  quatsliders = labelslidergrid!(sceneinfo.scene, ["β₀", "β₁", "β₂", "β₃"], quatrange; formats = x -> format3decimal(x)) #, tellheight=false)
  
  anglerange = LinRange(-180.0:0.1:180.0)
  prvrange = LinRange(-1.0:0.001:1.0)
  prvsliders = labelslidergrid!(sceneinfo.scene, ["θ", "e₁", "e₂", "e₃"], (anglerange, prvrange, prvrange, prvrange), formats=[x -> format3decimal(x, unit) for unit in ("°", "", "", "")]) #, tellheight=false)
  
  mrprange = Ref(LinRange(-1.0:0.001:1.0))
  mrpsliders = labelslidergrid!(sceneinfo.scene, ["σ₁", "σ₂", "σ₃"], mrprange, formats = x -> format3decimal(x)) #, tellheight=false)
  
  angvelrange = Ref(LinRange(-90.0:0.1:90.0))
  angvelsliders = labelslidergrid!(sceneinfo.scene, ["ω₁", "ω₂", "ω₃"], angvelrange, formats = x -> format3decimal(x, "°/s")) #, tellheight=false)
  
  slidersgrid = sceneinfo.layout[1,1] = GridLayout()
  slidersgrid[end+1, 1] = vehiclemenu
  slidersgrid[end+1, 1] = titled!(sceneinfo.scene, yprsliders.layout, "3 - 2 - 1 Euler Angles")
  slidersgrid[end+1, 1] = titled!(sceneinfo.scene, quatsliders.layout, "Euler Parameters/Quaternion")
  slidersgrid[end+1, 1] = titled!(sceneinfo.scene, prvsliders.layout, "Principal Rotation Vector PRV")
  slidersgrid[end+1, 1] = titled!(sceneinfo.scene, mrpsliders.layout, "Modified Rodrigues Parameters MRP")
  slidersgrid[end+1, 1] = titled!(sceneinfo.scene, angvelsliders.layout, "Angular Velocity 𝛚")
  
  
  for lsg in (yprsliders, quatsliders, prvsliders, mrpsliders, angvelsliders)
    colsize!(lsg.layout, 1, Fixed(20))
    colsize!(lsg.layout, 2, Fixed(300))
    colsize!(lsg.layout, 3, Fixed(80))
  end
  
  # DCM
  dcmaxis = slidersgrid[end+1, 1] = Axis(sceneinfo.scene, title="DCM")
  hidedecorations!.([dcmaxis, ]) #cbaraxis])
  
  rowsize!(slidersgrid, slidersgrid.nrows, Aspect(1,1))

  # MRP plot
  mrpaxis = slidersgrid[end+1, 1] = Axis(sceneinfo.scene, title="MRP")
  rowsize!(slidersgrid, slidersgrid.nrows, Aspect(1,0.5))
  
  # Zero button
  slidersgrid[end+1,1] = zerobutton = Button(sceneinfo.scene, label="Zero", tellwidth=false)
  
  # View & Model
  uiview = UIView(sceneinfo, vehiclemenu, yprsliders, quatsliders, prvsliders, mrpsliders, angvelsliders, dcmaxis, mrpaxis, zerobutton, spacecraft)
  uimodel = UIModel()
  hm = heatmap!(dcmaxis, lift(x -> -C321d(x...)[end:-1:1,:], uimodel.ypr_deg), colormap=colorschemes[:bwr])
  hm.colorrange = (-1.0, 1.0)
  
  for i in 1:3, j in 1:3
    # ##@debug i,j
    text!(dcmaxis.scene, lift(x -> format3decimal(C321d(x...)[end:-1:1,:][i,j]), uimodel.ypr_deg), position=(i-0.5,j-0.5), textsize=0.2, align = (:center, :center), font="Calibri", color=:black)
  end


  on(vehiclemenu.selection) do s
    @info "on(vehiclemenu.selection)"
    @show s
    uiview.mesh.mesh[] = s
    # update!(uiview.sceneinfo.scene.scene)
  end

  vehiclemenu.i_selected[] = 1 # force selection
  
  on(zerobutton.clicks) do btn
    ##@debug "Zero"
    reset_rotation!(uimodel)
    update_view!(uiview, uimodel)
  end
  
  for (sliders, update_function, range) in [
    [uiview.yprsliders.sliders, update_ypr!, 1:3],
    [uiview.quatsliders.sliders, update_quat!, 1:4],
    [uiview.prvsliders.sliders, update_prv!, 1:4],
    [uiview.mrpsliders.sliders, update_mrp!, 1:3],
    [uiview.angvelsliders.sliders, update_angvel!, 1:3],
    ]
    for i in range
      on(sliders[i].value) do x
        # @debug "On $(sliders[i]), $x, $(ui_locked(uiview))"
        if !ui_locked(uiview)
          ##@debug sliders[i].value
          lock_ui!(uiview)
          update_function(uimodel, uiview)
          update_view!(uiview, uimodel)
          unlock_ui!(uiview)
        end
      end
    end
  end

  ##@debug uiview.ui_locked
  
  # Forces an update
  set_close_to!(uiview.quatsliders.sliders[1], 1.0)
  return uiview, uimodel
end



function interact(uiview, uimodel)
  ##@debug uiview.ui_locked
  @async_showerr begin
  # # Threads.@spawn begin
  #   try
  #     @debug "Entering @async block"
  #     #@debug "uiview.ui_locked = $(uiview.ui_locked)"
      interaction_loop(uiview, uimodel)
    #   @debug "Exiting @async block"
    # catch e
    #   @show e
    #   @error "foo $e"
    # end
  end
end


function interaction_loop(uiview::UIView, uimodel::UIModel)
  uimodel.runloop && error("Loop already running")
  uimodel.runloop = true
  t = 0.0
  t_prev = 0.0
  dt = 0.01
  # ##@debug isopen(uiview.sceneinfo.scene)
  iloop = "interaction_loop"
  waiting = "$(iloop): Waiting for scene.."
  #@debug waiting
  while !isopen(uiview.sceneinfo.scene.scene) # wait for screen to be open
    #@debug waiting
    sleep(1)
  end
  #@debug "$(iloop): Scene ready"
  t = time()
  t0 = t
  loopcount = 0
  dt_buf = CircularBuffer{Float64}(100)
  while uimodel.runloop && isopen(uiview.sceneinfo.scene.scene)
    uiview.sceneinfo.infotext.input_args[1][] = "t = $(@sprintf("%.1f", t - t0)) s; FPS = $(@sprintf("%.1f", 1/max(0.01, mean(dt_buf))))"
    # loopcount > 100 && error("test error")
    loopcount += 1
    # @debug "Top of interaction loop t = $(t)"
    #@debug "uiview.ui_locked = $(uiview.ui_locked); $(ui_locked(uiview))"
    t_prev = t
    t = time()
    dt_meas = t - t_prev
    push!(dt_buf, dt_meas)
    @debug "Locking UI"
    lock_ui!(uiview)
    # #@debug "After lock_ui"
    # @show uimodel.ypr_deg
    σ = DCM2MRP(C321d(uimodel.ypr_deg[]...))
    # @debug "σ = $(σ)"
    # # # σ = mrp_shadow(σ)
    # # ##@debug uimodel
    ω = deg2rad.(uimodel.omega_dps)
    @debug "σ = $(σ), ω = $(ω)"
    x = 2/0.0
    # error("test")
    σ̇ = dσdt_mrp(σ, ω)
    # @debug σ̇
    @debug "σ = $(σ), $(ω), σ̇ = $σ̇"
    σ += σ̇*dt_meas
    # # # σ = mrp_shadow(σ)
    # # #@debug "X $(σ)"
    C = MRP2DCM(σ)
    # # # C = [1. 0 0; 0 1 0; 0 0 1]
    # # #@debug "Y $(σ) $(C)"
    # # ##@debug C
    # # #@debug "Before ypr update"
    ypr = DCM2ypr_deg(C)
    # # ##@debug ypr, uimodel.ypr_deg
    uimodel.ypr_deg[] = ypr
    # # ##@debug ypr, uimodel.ypr_deg
    # @debug "Before update_view!"
    update_view!(uiview, uimodel)
    # @debug "After update_view!"
    unlock_ui!(uiview)
    @debug "After unlock_ui!"
    sleep(0.8*dt)
    @debug "Bottom of run loop"
  end
  @warn "$iloop: exiting"
end

function animatefromdata!(t_data, σ, uiview::UIView, uimodel::UIModel)
  uimodel.runloop = false
  N = length(t_data)
  @assert size(σ) == (3, N)
  t0 = time()
  t = t0
  dt = 0.01
  i = 1
  while i < N
    # ##@debug i, t, σ[:,i]
    while t < t_data[i]
      sleep(dt)
      t = time()
    end
    t = time() - t0
    while i < N && t_data[i] < t
      i +=1 
    end
    C = MRP2DCM(σ[:,i])
    lock_ui!(uiview)
    uimodel.ypr_deg[] = DCM2ypr_deg(C)
    update_view!(uiview, uimodel)
    unlock_ui!(uiview)
  end
end

# function main()
#   #@debug "main()"
#   uiview, uimodel = attitude_viz_scene()
#   show_scene(uiview.sceneinfo.scene)
#   interact(uiview, uimodel)
#   #@debug "Set uiview.uimodel.runloop to false to exit interaction"
#   (;uiview, uimodel)
# end

end
