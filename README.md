# SpacecraftVisualization

This is a Julia package for playing around with visualizing spacecraft in 3D. It is very much a work in progress and for the time being probably shouldn't be used for anything other than self-study (which is what I'm doing with it).

![Animation](./docs/animation.gif)

## Requirements
I'm developing this with the latest Julia (currently 1.6.0-beta1) and usually am on the master branch of Makie/AbstractPlotting and GLMakie.

## Features
Currently there is a basic visualization of the attitude of a spacecraft (a model of Voyager is used) with the ability to change the attitude interactively using sliders. This can be done using one of several attitude parametrizations, currently 3-2-1 Euler Angles, Euler Parameters (a.k.a. Quaternions), and Modified Rodrigues Parameters.

Changing the sliders in one parametrization will change all the others to keep them consistent. In some cases that includes sliders within the same parametrization; for example the quaternion representation due to the quaternion being continuously renormalized.

There is also a readout of the corresponding Direction Cosine Matrix (DCM).

Sliders allow for interactive input of and angular velocity (expressed in body frame). The spacecraft will rotate accordingly in the 3D view and the attitude sliders will update.

## Known Issues
- due to renormalization, dragging to the end on a quaternion or PRV slider doesn't always result in the slider being at its limit value
- CPU/GPU utilization is very high, to the point other applications become unusable. Probably this issue: https://github.com/JuliaPlots/Makie.jl/issues/678

## Roadmap
- remove dependency on my personal SpacecraftKinematics package in favor of [Rotations.jl](https://github.com/JuliaGeometry/Rotations.jl)
- migrate to [JSServe.jl](https://github.com/SimonDanisch/JSServe.jl)/[WGLMakie.jl](https://github.com/JuliaPlots/WGLMakie.jl) (or add support in addition to GLMakie)?
- allow different meshes to be used other than Voyager
- API for animations based on simulation time history output
- API to allow for use within an interactive simulator
- better decoupling of main 3D visualization from attitude widgets
- more modularity in general
- indications of attitude relative to celestial coordinates, e.g.
  - First Point of Aries
  - navigational stars
- orbit visualization
- allow attitude changes using a widget in the 3D view
- 'turntable' rotation when mouseup while cursor is still moving during a drag operation
- isometric view
- keyboard shortcuts
  - change view (top/left etc.)
  - reset rotation
- add tests
- add CI
- better Voyager model (maybe [this](https://3dwarehouse.sketchup.com/model/48645da92be302fd2e5ae31c6c631a39/Voyager))
- inertial frame coordinate triad overlaid in corner of 3D view
- resolve performance issue (TODO: ref GitHub issue..)
- show indication of PRV vector in 3D view
- hide angular velocity vector in 3D view when not rotating
- ability to manually enter parameters

## Acknowlegments
- The excellent Coursera course [Kinematics: Describing the Motions of Spacecraft](https://www.coursera.org/learn/spacecraft-dynamics-kinematics) with Prof. Hanspeter Schaub of the University of Colorado Boulder was very useful in gaining an understanding of different attitude parametrizations.
- Voyager 3d model from [GrabCAD](https://free3d.com/3d-model/voyager-2-v1--717773.html)
- New Horizons 3d model assembled using files from [NASA](https://nasa3d.arc.nasa.gov/detail/new-horizons)