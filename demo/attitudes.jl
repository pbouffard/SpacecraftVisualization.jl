module AttitudeVisualizationApp

using SpacecraftVisualization
using SpacecraftVisualization.AttitudeVisualization
using SpacecraftVisualization.Utils

function main()
  uiview, uimodel = attitude_viz_scene()
  show_scene(uiview.sceneinfo.scene)
  interact(uiview, uimodel)
  (;uiview, uimodel)
end

end

AttitudeVisualizationApp.main()
